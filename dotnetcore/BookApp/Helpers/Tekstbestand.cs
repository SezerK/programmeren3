﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.Helpers
{
    public class Tekstbestand
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private string melding;

        public string Melding
        {
            get { return melding; }
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set
            {
                this.fileName = value;
            }
        }

        public bool Lees()
        {
            // we gaan ervan uit dat er iets misloopt
            bool result = false;
            try
            {
                // Creëer een instantie van de StreamReader klasse om 
                // een bestand in te lezen. 
                // Het using statement sluit ook de StreamReader. 
                using (StreamReader sr = new StreamReader(this.fileName))
                {
                    // Lees tot het einde van het bestand
                    // this.Text = sr.ReadToEnd();
                    // sr.ReadToEnd(); will get you everything, de \n en \r incluis. 
                    // The easiest way to get rid of most unneeded whitespace is 
                    // to read it line-by - line and then simply.Trim() every line.
                    // zoals hieronder
                    string line;
                    List<string> lines = new List<string>();
                    while ((line = sr.ReadLine()) != null)
                        lines.Add(line.Trim());

                    this.Text = string.Join("\n", lines);
                    this.melding = String.Format(
                    "Het bestand met de naam {0} is ingelezen.Het bevat {1} karakters en {2} woorden en {3} regels.",
                    this.fileName, this.text.Length, this.text.Split(' ').Length, this.text.Split('\n').Length);
                    result = true;
                }
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                this.melding = $"Kan het bestand met de naam {this.fileName} niet inlezen.\nFoutmelding {e.Message}.";
            }
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.Dal
{
    public interface IBook
    {
        // Property signatures:
        // Een Postcode BLL object om de opgehaalde waarden
        // in op te slagen
        Bll.Book Book { get; set; }
        // Error message
        string Message { get; set; }
        string ConnectionString { get; set; }
        // method signatures
        bool Create();
        bool ReadAll();
    }
}

﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            // create service provider
            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            serviceProvider.GetService<App>().Run();
            Console.ReadLine();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            // build configuration
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build();
            serviceCollection.AddOptions();
            serviceCollection.Configure<AppSettings>(configuration.GetSection("Configuration"));
            // add services
            serviceCollection.AddSingleton<Dal.IBook>(p => new Dal.BookJson(new Bll.Book()));
            // add app
            serviceCollection.AddTransient<App>();
        }

        static void TryOut(ServiceProvider serviceProvider)
        {
            Console.WriteLine("De Book App Generic");
            var bookDal = serviceProvider.GetService<Dal.IBook>();
            bookDal.ReadAll();
            Console.WriteLine(bookDal.Message);
            View.BookConsole view = new View.BookConsole(bookDal.Book);
            view.List();
            // serialize
            bookDal.Create();
            Console.WriteLine(bookDal.Message);
        }

        static void TryOutCsv()
        {
            Console.WriteLine("De Book App CSV");
            Bll.Book book = new Bll.Book();
            Dal.BookCsv bookCsv = new Dal.BookCsv(book);
            // de seperator staat standaard op ;
            // in het Book.csv bestand is dat |
            bookCsv.Separator = '|';
            bookCsv.Book = book;
            bookCsv.ReadAll();
            Console.WriteLine(bookCsv.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            // serialize books met een andere separator
            // naar ander bestand
            bookCsv.ConnectionString = "Data/Book.csv";
            bookCsv.Create(';');
            Console.WriteLine(bookCsv.Message);
        }

        static void TryOutXml()
        {
            Console.WriteLine("De Book App XML");
            Bll.Book book = new Bll.Book();
            Dal.BookXml bookXml = new Dal.BookXml(book);
            bookXml.Book = book;
            bookXml.ReadAll();
            Console.WriteLine(bookXml.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            // serialize naar ander bestand
            bookXml.ConnectionString = "Data/Book.xml";
            bookXml.Create();
            Console.WriteLine(bookXml.Message);
        }

        static void TryOutJson()
        {
            Console.WriteLine("De Book App Json");
            Bll.Book book = new Bll.Book();
            Dal.BookJson bookJson = new Dal.BookJson(book);
            bookJson.Book = book;
            bookJson.ReadAll();
            Console.WriteLine(bookJson.Message);
            View.BookConsole view = new View.BookConsole(book);
            view.List();
            // serialize naar ander bestand
            bookJson.ConnectionString = "Data/Book.json";
            bookJson.Create();
            Console.WriteLine(bookJson.Message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.View
{
    class BookConsole
    {
        public Bll.Book Model { get; set; }

        public BookConsole(Bll.Book book)
        {
            Model = book;
        }

        public void List(Bll.Book book)
        {
            Model = book;
        }

        public void List()
        {
            foreach (Bll.Book book in Model.List)
            {
                // One of the most versatile and useful additions to the C# language in version 6 
                // is the null conditional operator ?.Post           
                Console.WriteLine("Title: {0}\tYear:{1}\tCity:{2}\tPublisher:{3}\tAuthor:{4}\tTranslator:{5}\tComment:{6}",
                    book?.Title,
                    book?.Year,
                    book?.City,
                    book?.Publisher,
                    book?.Author,
                    book?.Translator,
                    book?.Comment);
            }
        }
    }
}

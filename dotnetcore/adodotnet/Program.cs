﻿using System;

namespace AdoDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met ADO.NET in .NET Core!");
            //Learning.TestMySqlConnector();
            //Learning.ReflectPropertiesTryOut();
            //Learning.LearnDotNetCoreConfigurationApi();
            Learning.FricFracDalTest();
            Console.ReadKey();
        }
    }
}

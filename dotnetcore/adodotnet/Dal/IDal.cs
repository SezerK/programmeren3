﻿using System.Collections.Generic;
namespace FricFrac.Dal
{
    interface IDal<T>
    {
        string Message { get; }
        int RowCount { get; }
        T ReadOne(int id);
        int Create(T bll);
        int Update(T bll);
        List<T> ReadAll();
        List<T> ReadByName(string name);
        List<T> ReadLikeName(string name);
        List<T> ReadLikeXName(string name);
        int Delete(int id);
    }
}

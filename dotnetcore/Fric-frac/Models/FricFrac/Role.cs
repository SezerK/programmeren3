﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace FricFrac.Models.FricFrac
{
    public partial class Role
    {
        public Role()
        {
        }

        [Required]
        [StringLength(50)]
        [FromForm(Name = "Role-Name")]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Role-Id")]
        public int Id { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class Event
    {
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-Name")]
        public string Name { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-Location")]
        public string Location { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Event-Starts")]
        public DateTime? Starts { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Event-Ends")]
        public DateTime? Ends { get; set; }
        [Required]
        [StringLength(255)]
        [FromForm(Name = "Event-Image")]
        public string Image { get; set; }
        [Required]
        [StringLength(1024)]
        [FromForm(Name = "Event-Description")]
        public string Description { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-OrganiserName")]
        public string OrganiserName { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-OrganiserDescription")]
        public string OrganiserDescription { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-EventCategoryId")]
        public int? EventCategoryId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-EventTopicId")]
        public int? EventTopicId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-Id")]
        public int Id { get; set; }

        [ForeignKey("EventCategoryId")]
        public EventCategory EventCategory { get; set; }
        [ForeignKey("EventTopicId")]
        public EventTopic EventTopic { get; set; }
    }
}

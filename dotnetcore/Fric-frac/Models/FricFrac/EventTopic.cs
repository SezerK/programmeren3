﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class EventTopic
    {
        public EventTopic()
        {
        }

        [Required]
        [StringLength(120)]
        [FromForm(Name = "EventTopic-Name")]
        public string Name { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "EventTopic-Id")]
        public int Id { get; set; }
    }
}

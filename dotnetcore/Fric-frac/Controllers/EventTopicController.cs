﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class EventTopicController : Controller
    {

        private readonly User309Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventTopicController(User309Context dbContext)
        {
            this.dbContext = dbContext;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventTopic Index";
            return View(dbContext.EventTopic.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic Inserting One";
            return View(dbContext.EventTopic.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventTopic Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var eventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (eventTopic == null)
            {
                return NotFound();
            }
            return View(eventTopic);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.FricFrac.EventTopic EventTopic = new Models.FricFrac.EventTopic();
            EventTopic.Name = Request.Form["EventTopic-Name"];

            ViewBag.Message = "Insert een event topic in de database";
            dbContext.EventTopic.Add(EventTopic);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventTopic);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.EventTopic EventTopic)
        {
            ViewBag.Message = "Insert een event topic in de database";
            dbContext.EventTopic.Add(EventTopic);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventTopic);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een event topic in de database";
            if (id == null)
            {
                return NotFound();
            }

            var eventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (eventTopic == null)
            {
                return NotFound();
            }
            return View(eventTopic);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventTopic eventTopic)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(eventTopic);
                    dbContext.SaveChanges();
                    return View("ReadingOne", eventTopic);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventTopic.Any(e => e.Id == eventTopic.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", eventTopic);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var eventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (eventTopic == null)
            {
                return NotFound();
            }
            dbContext.EventTopic.Remove(eventTopic);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}

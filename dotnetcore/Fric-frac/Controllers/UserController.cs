﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FricFrac.Models.FricFrac;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class UserController : Controller
    {
        private readonly User309Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public UserController(User309Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac User Index";
            return View(dbContext.User.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Message = "Insert een user in de database";
            ViewBag.Person = dbContext.Person.ToList();
            ViewBag.Role = dbContext.Role.ToList();
            return View();
        }

        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac User Reading One";
            return View();
        }

        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac User Updating One";
            if (id == null)
            {
                return NotFound();
            }
            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            ViewBag.Role = dbContext.Role.ToList();
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);
            ViewBag.Person = dbContext.Person.ToList();
            return View(user);
        }

        public IActionResult Cancel()
        {
            ViewBag.Title = "Fric-frac User Cancel";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.User user)
        {
            ViewBag.Message = "Insert een user in de database";
            dbContext.User.Add(user);
            dbContext.SaveChanges();
            return View("Index", dbContext.User);
        }
        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een user in de database";
            if (id == null)
            {
                return NotFound();
            }

            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);

            return View(user);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.User user)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(user);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.User.Any(e => e.Id == user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.User);
        }

        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            dbContext.User.Remove(user);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}

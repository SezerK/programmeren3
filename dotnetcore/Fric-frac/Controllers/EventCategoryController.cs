﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class EventCategoryController : Controller
    {

        private readonly User309Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventCategoryController(User309Context dbContext)
        {
            this.dbContext = dbContext;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventCategory Index";
            return View(dbContext.EventCategory.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory Inserting One";
            return View(dbContext.EventCategory.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventCategory Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var eventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventCategory == null)
            {
                return NotFound();
            }
            return View(eventCategory);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }


        [HttpPost]
        // rechtstreeks ophalen uit de payload
        public IActionResult InsertOneFromRequest()
        {
            Models.FricFrac.EventCategory EventCategory = new Models.FricFrac.EventCategory();
            EventCategory.Name = Request.Form["EventCategory-Name"];

            ViewBag.Message = "Insert een event categorie in de database";
            dbContext.EventCategory.Add(EventCategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.EventCategory EventCategory)
        {
            ViewBag.Message = "Insert een event categorie in de database";
            dbContext.EventCategory.Add(EventCategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een event categorie in de database";
            if (id == null)
            {
                return NotFound();
            }

            var eventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventCategory == null)
            {
                return NotFound();
            }
            return View(eventCategory);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventCategory eventCategory)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(eventCategory);
                    dbContext.SaveChanges();
                    return View("ReadingOne", eventCategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventCategory.Any(e => e.Id == eventCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", eventCategory);
        }
        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var eventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (eventCategory == null)
            {
                return NotFound();
            }
            dbContext.EventCategory.Remove(eventCategory);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}

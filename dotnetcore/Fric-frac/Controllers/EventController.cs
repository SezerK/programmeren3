﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class EventController : Controller
    {
        private readonly User309Context dbContext;
        // voeg constructor toe om geïnjecteerde DBContext 
        // te kunnen binnenkrijgen in deze klasse
        public EventController(User309Context dbContext)
        {
            this.dbContext = dbContext;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Event Index";
            return View(dbContext.Event.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Message = "Insert een event in de database";
            ViewBag.EventCategory = dbContext.EventCategory.ToList();
            ViewBag.EventTopic = dbContext.EventTopic.ToList();
            return View();
        }

        public IActionResult ReadingOne()
        {
            ViewBag.Title = "Fric-frac Event Reading One";
            return View();
        }

        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Event Updating One";
            if (id == null)
            {
                return NotFound();
            }
            var evnt = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (evnt == null)
            {
                return NotFound();
            }
            evnt.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == evnt.EventCategoryId);
            ViewBag.EventCategory = dbContext.EventCategory.ToList();
            evnt.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == evnt.EventTopicId);
            ViewBag.EventTopic = dbContext.EventTopic.ToList();
            return View(evnt);
        }

        public IActionResult Cancel()
        {
            ViewBag.Title = "Fric-frac Event Cancel";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Event evnt)
        {
            ViewBag.Message = "Insert een event in de database";
            dbContext.Event.Add(evnt);
            dbContext.SaveChanges();
            return View("Index", dbContext.Event);
        }
        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een event in de database";
            if (id == null)
            {
                return NotFound();
            }

            var evnt = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (evnt == null)
            {
                return NotFound();
            }
            evnt.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == evnt.EventCategoryId);
            evnt.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == evnt.EventTopicId);

            return View(evnt);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Event evnt)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(evnt);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Event.Any(e => e.Id == evnt.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.Event);
        }

        // GET: Supplier/Delete/5
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var evnt = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (evnt == null)
            {
                return NotFound();
            }
            dbContext.Event.Remove(evnt);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Fric_frac.Models;

namespace Fric_frac.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            // Wordt gebruikt in het head->title element
            // van de Master Page
            ViewBag.Title = "Home";
            return View();
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult AboutMe()
        {
            ViewData["Message"] = "Korte biografie";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

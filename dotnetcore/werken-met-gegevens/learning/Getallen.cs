﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace learning
{
    class Getallen
    {

        public string FormatNumericSample()
        {
            double value;
            StringBuilder numberFormatted = new StringBuilder();

            value = 123;
            numberFormatted.AppendFormat("Een voorbeeld met ToString: {0}.", value.ToString("00000"));
            numberFormatted.AppendLine(String.Format("{0:00000}", value));
            // Displays 00123 

            value = 1.2;
            numberFormatted.AppendLine(value.ToString("0.00", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                        "{0:0.00}", value));
            // Displays 1.20

            numberFormatted.AppendLine(value.ToString("00.00", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:00.00}", value));
            // Displays 01.20

            CultureInfo daDK = CultureInfo.CreateSpecificCulture("da-DK");
            numberFormatted.AppendLine(value.ToString("00.00", daDK));
            numberFormatted.AppendLine(String.Format(daDK, "{0:00.00}", value));
            // Displays 01,20 

            CultureInfo beNL = CultureInfo.CreateSpecificCulture("nl-BE");
            numberFormatted.AppendFormat("Het getal 123 in Belgisch Nederlands: {0}.\n", value.ToString("00.00", daDK));

            value = .56;
            numberFormatted.AppendLine(value.ToString("0.0", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0.0}", value));
            // Displays 0.6 

            value = 1234567890;
            numberFormatted.AppendLine(value.ToString("0,0", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0,0}", value));
            // Displays 1,234,567,890      

            CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
            numberFormatted.AppendLine(value.ToString("0,0", elGR));
            numberFormatted.AppendLine(String.Format(elGR, "{0:0,0}", value));
            // Displays 1.234.567.890 

            value = 1234567890.123456;
            numberFormatted.AppendLine(value.ToString("0,0.0", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0,0.0}", value));
            // Displays 1,234,567,890.1   

            value = 1234.567890;
            numberFormatted.AppendLine(value.ToString("0,0.00", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                        "{0:0,0.00}", value));
            // Displays 1,234.57 

            value = 1.2;
            numberFormatted.AppendLine(value.ToString("#.##", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#.##}", value));
            // Displays 1.2 

            value = 123;
            numberFormatted.AppendLine(value.ToString("#####"));
            numberFormatted.AppendLine(String.Format("{0:#####}", value));
            // Displays 123 

            value = 123456;
            numberFormatted.AppendLine(value.ToString("[##-##-##]"));
            numberFormatted.AppendLine(String.Format("{0:[##-##-##]}", value));
            // Displays [12-34-56] 

            value = 1234567890;
            numberFormatted.AppendLine(value.ToString("#"));
            numberFormatted.AppendLine(String.Format("{0:#}", value));
            // Displays 1234567890

            numberFormatted.AppendLine(value.ToString("(###) ###-####"));
            numberFormatted.AppendLine(String.Format("{0:(###) ###-####}", value));
            // Displays (123) 456-7890

            value = 1.2;
            numberFormatted.AppendLine(value.ToString("0.00", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:0.00}", value));
            // Displays 1.20

            numberFormatted.AppendLine(value.ToString("00.00", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:00.00}", value));
            // Displays 01.20

            numberFormatted.AppendLine(value.ToString("00.00",
                                CultureInfo.CreateSpecificCulture("da-DK")));
            numberFormatted.AppendLine(String.Format(CultureInfo.CreateSpecificCulture("da-DK"),
                                "{0:00.00}", value));
            // Displays 01,20 

            value = .086;
            numberFormatted.AppendLine(value.ToString("#0.##%", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#0.##%}", value));
            // Displays 8.6% 

            value = 86000;
            numberFormatted.AppendLine(value.ToString("0.###E+0", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                "{0:0.###E+0}", value));
            // Displays 8.6E+4
            value = 1234567890;
            numberFormatted.AppendLine(value.ToString("#,,", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,,}", value));
            // Displays 1235   

            numberFormatted.AppendLine(value.ToString("#,,,", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,,,}", value));
            // Displays 1  

            numberFormatted.AppendLine(value.ToString("#,##0,,", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#,##0,,}", value));
            // Displays 1,235
            value = .086;
            numberFormatted.AppendLine(value.ToString("#0.##%", CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:#0.##%}", value));
            // Displays 8.6%   
            value = .00354;
            string perMilleFmt = "#0.## " + '\u2030';
            numberFormatted.AppendLine(value.ToString(perMilleFmt, CultureInfo.InvariantCulture));
            numberFormatted.AppendLine(String.Format(CultureInfo.InvariantCulture,
                                            "{0:" + perMilleFmt + "}", value));
            // Displays 3.54‰
            return numberFormatted.ToString();
        }

    }
}

﻿using System;
using System.Text;

namespace learning
{
    class Program
    {
        enum Days { Sat, Sun, Mon, Tue, Wed, Thu, Fri };
        static void Main(string[] args)
        {
            //TestKarakters();
            //TestNumberFormatting();
            //TestSamenvoegen();
            //TestVergelijken();
            //TestEscapeTekens();
            //StringVsStringbuilder();
            //TestInterpolatie();
            //TestEnumeratie();

            //Bewijzen dat struct een by value is en class een by reference
            StructVersusClass svc = new StructVersusClass();
            EenClass testClass = new EenClass();
            EenStruct testStruct = new EenStruct();

            testClass.veranderdOfNiet = "niet veranderd";
            testStruct.veranderdOfNiet = "niet veranderd";

            svc.ClassOproepen(testClass);
            svc.StructOproepen(testStruct);

            Console.WriteLine("De class string is {0}.", testClass.veranderdOfNiet);
            Console.WriteLine("De struct string is {0}.", testStruct.veranderdOfNiet);

            Console.ReadKey();
        }

        static void TestKarakters()
        {
            char ch2 = '2';
            string str2 = "Upper Case";
            Console.WriteLine(Char.GetUnicodeCategory('a'));    // Output: "LowercaseLetter"
            Console.WriteLine(Char.GetUnicodeCategory(ch2));    // Output: "DecimalDigitNumber"
            Console.WriteLine(Char.GetUnicodeCategory(str2, 6));    // Output: "UppercaseLetter"

            char chA = 'A';
            char ch1 = '1';
            string str = "test string";

            Console.WriteLine(chA.CompareTo('B'));      // Output: "-1" (meaning 'A' is 1 less than 'B')
            Console.WriteLine(chA.Equals('A'));     // Output: "True"
            Console.WriteLine(Char.GetNumericValue(ch1));   // Output: "1"
            Console.WriteLine(Char.IsControl('\t'));    // Output: "True"
            Console.WriteLine(Char.IsDigit(ch1));       // Output: "True"
            Console.WriteLine(Char.IsLetter(','));      // Output: "False"
            Console.WriteLine(Char.IsLower('u'));       // Output: "True"
            Console.WriteLine(Char.IsNumber(ch1));      // Output: "True"
            Console.WriteLine(Char.IsPunctuation('.')); // Output: "True"
            Console.WriteLine(Char.IsSeparator(str, 4));    // Output: "True"
            Console.WriteLine(Char.IsSymbol(' '));      // Output: "True"
            Console.WriteLine(Char.IsWhiteSpace(str, 4));   // Output: "True"
            Console.WriteLine(Char.Parse("S"));     // Output: "S"
            Console.WriteLine(Char.ToLower('M'));       // Output: "m"
            Console.WriteLine('x'.ToString());		// Output: "x"
        }

        static void TesstVerbatimStrings()
        {
            string file = @"C:\Users\Sezer\Desktop\programmeren3\dotnetcore";
        }

        static void TestSamenvoegen()
        {
            string s = "Hello" + " " + "world!";

            Console.WriteLine(s);
        }

        static void TestVergelijken()
        {
            //Manier 1
            string s = "";

            string color1 = "red";
            string color2 = "green";
            string color3 = "red";

            if (color1 == color3)
            {
                s = "Equal\n";
            }
            if (color1 != color2)
            {
                s = "Not equal\n";
            }

            Console.WriteLine(s);

            //Manier 2
            s = "Resultaat van Equals: " + s.Equals("Eva").ToString() + "\n";

            Console.WriteLine(s);

            // Manier 3
            string string1 = "ABC";
            string string2 = "abc";

            int result = string1.CompareTo(string2);

            if (result > 0)
            {
                s = String.Format("{0} is greater than {1}\n", string1, string2);
            }
            else if (result == 0)
            {
                s = String.Format("{0} is equal to {1}\n", string1, string2);
            }
            else if (result < 0)
            {
                s = String.Format("{0} is less than {1}\n", string1, string2);
            }

            Console.WriteLine(s);
        }

        static void TestEscapeTekens()
        {
            Console.WriteLine("Dit laat de Bell sound afspelen\a");
            Console.WriteLine("Volgend lijn\nHoi");
        }

        static void StringVsStringbuilder()
        {
            /*//Met string
            string a = "Sezer";
            string b = "Kulac";
            string naam = a + " " + b;

            Console.WriteLine("Met string {0}", naam);

            //Met stringbuilder
            StringBuilder sb = new StringBuilder();
            sb.Append("Sezer Kulac");

            Console.WriteLine("Met stringbuilder {0}", sb.ToString());*/

            //Met string
            int timerS = DateTime.Now.Millisecond;
            string x = "";

            for (int i = 0; i < 100000; i++)
            {
                x += ".";
            }

            int timerString = DateTime.Now.Millisecond;
            Console.WriteLine("Het heeft de string {0} ms geduurd om dit uit te voeren.", timerString - timerS);

            //Met stringbuilder
            int timerSB = DateTime.Now.Millisecond;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 100000; i++)
            {
                sb.Append(".");
            }
            x = sb.ToString();

            int timerStringBuilder = DateTime.Now.Millisecond;
            Console.WriteLine("Het heeft de stringbuilder {0} ms geduurd om dit uit te voeren.", timerStringBuilder - timerSB);

        }

        static void TestNumberFormatting()
        {
            Getallen getal = new Getallen();
            Console.WriteLine(getal.FormatNumericSample());
        }

        static void TestInterpolatie()
        {
            //Composite formating
            string item = "bread";
            decimal amount = 2.25m;
            Console.WriteLine("{0,-10}{1:C}", item, amount);

            //Interpolatie
            Console.WriteLine($"{item}      {amount}");
        }

        static void TestEnumeratie()
        {
            int x = (int)Days.Sun;
            int y = (int)Days.Fri;
            Console.WriteLine("Sun = {0}", x);
            Console.WriteLine("Fri = {0}", y);
        }

    }
}

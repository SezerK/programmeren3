﻿using System;
using System.Collections.Generic;
using System.Text;

namespace learning
{
    class EenClass
    {
        public string veranderdOfNiet;
    }

    struct EenStruct
    {
        public string veranderdOfNiet;
    }

    class StructVersusClass
    {

        public void ClassOproepen(EenClass eenClass)
        {
            eenClass.veranderdOfNiet = "veranderd";
        }

        public void StructOproepen(EenStruct eenStruct)
        {
            eenStruct.veranderdOfNiet = "veranderd";
        }

    }
}

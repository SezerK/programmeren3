﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {

        private ConsoleColor kleur;
        private string teken;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }

        public string Teken
        {
            get { return teken; }
            set
            {
                teken = value;
            }
        }

        //Lijnen
        public static string Lijn(int lengte)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            return new string('-', lengte);
        }

        public static void Lijn(int lengte, ConsoleColor kleur)
        {
            for (int i = 0; i < lengte; i++)
            {
                Console.ForegroundColor = kleur;
                Console.Write('-');
            }
            Console.WriteLine();
        }

        public static void Lijn(int lengte, string teken)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            for(int i = 0; i < lengte; i++)
            {
                Console.Write(teken);
            }
            Console.WriteLine();
        }

        public static void Lijn(int lengte, ConsoleColor kleur, string teken)
        {
            for (int i = 0; i < lengte; i++)
            {
                Console.ForegroundColor = kleur;
                Console.Write(teken);
            }
            Console.WriteLine();
        }

        //Rechthoeken
        public static void Rechthoek(int basis, int hoogte)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            for (int i = 1; i <= hoogte; i++)
            {
                for (int j = 1; j <= basis; j++)
                {
                    Console.Write(".");
                }

                Console.WriteLine();
            }
        }

        public static void Rechthoek(int basis, int hoogte, ConsoleColor kleur)
        {
            for (int i = 1; i <= hoogte; i++)
            {
                for (int j = 1; j <= basis; j++)
                {
                    Console.ForegroundColor = kleur;
                    Console.Write(".");
                }

                Console.WriteLine();
            }
        }

        public static void Rechthoek(int basis, int hoogte, string teken)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            for (int i = 1; i <= hoogte; i++)
            {
                for (int j = 1; j <= basis; j++)
                {
                    Console.Write(teken);
                }

                Console.WriteLine();
            }
        }

        public static void Rechthoek(int basis, int hoogte, ConsoleColor kleur,  string teken)
        {
            for (int i = 1; i <= hoogte; i++)
            {
                for (int j = 1; j <= basis; j++)
                {
                    Console.ForegroundColor = kleur;
                    Console.Write(teken);
                }

                Console.WriteLine();
            }
        }

        //Driehoeken
        public static void Driehoek(int basis)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            for (int i = 1; i <= basis; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(".");
                }
                Console.WriteLine();
            }
        }

        public static void Driehoek(int basis, ConsoleColor kleur)
        {
            for (int i = 1; i <= basis; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.ForegroundColor = kleur;
                    Console.Write(".");
                }
                Console.WriteLine();
            }
        }

        public static void Driehoek(int basis, string teken)
        {
            Console.ForegroundColor = ConsoleColor.Gray;
            for (int i = 1; i <= basis; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(teken);
                }
                Console.WriteLine();
            }
        }

        public static void Driehoek(int basis, ConsoleColor kleur, string teken)
        {
            for (int i = 1; i <= basis; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.ForegroundColor = kleur;
                    Console.Write(teken);
                }
                Console.WriteLine();
            }
        }
    }
}

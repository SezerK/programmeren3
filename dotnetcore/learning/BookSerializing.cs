﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DotNetCore.Learning
{
    public class BookSerializing
    {
        public static List<Book> DeserializeFromXml()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Book[]));
            Book book = new Book();
            StreamReader file = new System.IO.StreamReader(@"Data/Book.xml");
            Book[] books = (Book[])serializer.Deserialize(file);
            file.Close();
            // array converteren naar List
            return new List<Book>(books);
        }

        public static string SerializeListToCsv(List<Book> list, string separator)
        {
            string fileName = @"Data/Books2.csv";
            string message;
            try
            {
                TextWriter writer = new StreamWriter(fileName);
                foreach (Book item in list)
                {
                    // One of the most versatile and useful additions to the C# language in version 6
                    // is the null conditional operator ?.           
                    writer.WriteLine("{0}{1}{2}{3}{4}{5}{6}",
                        item?.Title,
                        item?.Year,
                        item?.City,
                        item?.Publisher,
                        item?.Author,
                        item?.Translator,
                        item?.Comment,
                        separator);
                }
                message = $"Het bestand met de naam {fileName} is gemaakt!";
            }
            catch (Exception e)
            {
                // Melding aan de gebruiker dat iets verkeerd gelopen is.
                // We gebruiken hier de nieuwe mogelijkheid van C# 6: string interpolatie
                message = $"Kan het bestand met de naam {fileName} niet maken.\nFoutmelding {e.Message}.";
            }
            return message;
        }

        public static string DeserializeCsvToList()
        {
           Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
           bestand.FileName = @"Data/Books2.csv";
           bestand.Lees();
           return bestand.Text;
        }

        public static Book[] GetBookArray()
        {
            string[] lines = DeserializeCsvToList().Split('\n');
            Book[] books = new Book[lines.Length];
            int i = 0;
            foreach (string s in lines)
            {
                if (s.Length > 0)
                {
                    books[i++] = BookCsvToObject(s);
                }
            }
            return books;
        }

        public static List<Book> GetBookList()
        {

            string[] books = DeserializeCsvToList().Split('\n');
            List<Book> list = new List<Book>();
            foreach (string s in books)
            {
                if (s.Length > 0)
                {
                    list.Add(BookCsvToObject(s));
                }
            }
            return list;
        }

        public static Book BookCsvToObject(string line)
        {
            Book book = new Book();
            string[] values = line.Split('|');
            book.Title = values[0];
            book.Year = values[1];
            book.City = values[2];
            book.Publisher = values[3];
            book.Author = values[4];
            book.Translator = values[5];
            book.Comment = values[6];

            return book;
        }

        public static void ListBooks(List<Book> list)
        {
            foreach (Book book in list)
            {
                // One of the most versatile and useful additions to the C# language in version 6
                // is the null conditional operator ?.Post           
                Console.WriteLine("Title: {0}\tYear:{1}\tCity:{2}\tPublisher:{3}\tAuthor:{4}\tTranslator:{5}\tComment:{6}",
                    book?.Title,
                    book?.Year,
                    book?.City,
                    book?.Publisher,
                    book?.Author,
                    book?.Translator,
                    book?.Comment);
            }
        }

        public static void SerializeCsvToJson()
        {
            Book book = new Book();
            TextWriter writer = new StreamWriter(@"Data/Book.json");
            //De serializer werkt niet voor een generieke lijst en ook niet voor ArrayList
            List<Book> bookList = GetBookList();
            // static method SerilizeObject van Newtonsoft.Json
            string bookString = Newtonsoft.Json.JsonConvert.SerializeObject(bookList);
            writer.WriteLine(bookString);
            writer.Close();
        }

        public static List<Book> GetBookListFromJson()
        {
            Helpers.Tekstbestand bestand = new Helpers.Tekstbestand();
            bestand.FileName = @"Data/Book.json";
            bestand.Lees();
            List<Book> list = JsonConvert.DeserializeObject<List<Book>>(bestand.Text);
            return list;
        }

        public static List<Book> ShowBooks(){
            List<Book> books = DeserializeFromXml();
            return books;
        }
    }
}

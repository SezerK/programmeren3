﻿using System;
using System.Collections.Generic;
using Wiskunde.Meetkunde;

namespace DotNetCore.Learning
{
    class Program
    {
        static void Main(string[] args)
        {

            //DeserializeFromXml
            //BookSerializing.ListBooks(BookSerializing.GetBooksListFromXml());
            //BookSerializing.ListBooks(BookSerializing.DeserializeFromXml());

            //SerializeListToCsv
            //Console.WriteLine(BookSerializing.SerializeListToCsv(BookSerializing.GetBooksListFromXml(), "|"));

            //DeserializeCsvToList 
            //Console.WriteLine(BookSerializing.DeserializeCsvToList());
            //BookSerializing.ListBooks(BookSerializing.GetBookList());

            //SerializeListToJson
            //BookSerializing.SerializeCsvToJson();

            //DeserializeJsonToList
            //BookSerializing.ListBooks(BookSerializing.GetBookListFromJson());

            //Showbooks
            BookSerializing.ListBooks(BookSerializing.ShowBooks());







            //VORMEN OPGAVE
            /*Vormen vorm = new Vormen();
            string teken;
            ConsoleColor kleur;

            teken = "*";
            kleur = ConsoleColor.Green;

            vorm.Kleur = kleur;
            vorm.Teken = teken;

            //Lijnen
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Lijnen\n");

            Console.WriteLine(Vormen.Lijn(5));
            Vormen.Lijn(10, kleur);
            Vormen.Lijn(15, teken);
            Vormen.Lijn(20, kleur, teken);

            Console.WriteLine();

            //Rechthoeken
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Rechthoeken\n");

            Vormen.Rechthoek(7, 3);
            Vormen.Rechthoek(10, 4, kleur);
            Vormen.Rechthoek(13, 5, teken);
            Vormen.Rechthoek(15, 6, kleur, teken);

            Console.WriteLine();

            //Driehoeken
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Driehoeken\n");

            Vormen.Driehoek(3);
            Vormen.Driehoek(5, kleur);
            Vormen.Driehoek(7, teken);
            Vormen.Driehoek(10, kleur, teken);*/

            Console.ReadKey();
        }
    }
}
